# golang-generics

## Gitpod

This project is a [Gitpod](https://gitpod.io) project: if you open this project with GitPod, you'll get an entire GoLang `go1.18beta1` toolchain. Then you can start experimenting with Generics.

## OpenVSCode

> ref: https://www.gitpod.io/blog/openvscode-server-launch

```bash
docker run -it --init -p 3000:3000 k33g/openvscode-golang-beta:go1.18beta1_0.0.8
# then clone: https://gitlab.com/k33g_org/golang/golang-generics.git
``` 

## Run the sample

```bash
cd boxes
go run .
```
