package main

import "fmt"

type Box[T any] struct {
	value T
}

func (m *Box[T]) getValue() T {
  return m.value
}

func main() {

	bob:= Box[string]{value:"Bob Morane"}
	fmt.Println(bob.value)
	fmt.Println(bob.getValue())

	geekNumber:= Box[int64]{value:42}
	fmt.Println(geekNumber.value)
	fmt.Println(geekNumber.getValue())

}

